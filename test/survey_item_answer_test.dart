import 'dart:convert';

import 'package:flutter_surveys/model/survey_item_answer.dart';
import 'package:flutter_test/flutter_test.dart';

const SurveyItemAnswer EXAMPLE_SURVEY_ITEM_ANSWER_TEST_1 = SurveyItemAnswer(
    name: 'test name 1',
    label: 'test label 1',
    additionalData: <String, dynamic>{'numeric_value': 5, 'test_data': 'test 1'});

const SurveyItemAnswer EXAMPLE_SURVEY_ITEM_ANSWER_TEST_2 = SurveyItemAnswer(
    name: 'test name 2',
    label: 'test label 2',
    additionalData: <String, dynamic>{'numeric_value': 8, 'test_data': 'test 2'});

void main() {
  test('serialize and deserialize survey item answer', () {
    const SurveyItemAnswer answer = EXAMPLE_SURVEY_ITEM_ANSWER_TEST_1;

    final String encodedAnswer = jsonEncode(answer.toMap());

    final SurveyItemAnswer decodedAnswer = SurveyItemAnswer.fromMap(jsonDecode(encodedAnswer) as Map<String, dynamic>);

    expect(decodedAnswer, answer);
  });
}
