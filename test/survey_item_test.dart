import 'dart:convert';

import 'package:flutter_surveys/model/survey_item.dart';
import 'package:flutter_surveys/model/survey_item_answer.dart';
import 'package:flutter_test/flutter_test.dart';

import 'survey_item_answer_test.dart';

SurveyItem exampleSurveyItem1 = SurveyItem(
    name: 'test name 1',
    question: 'test question 1',
    possibleAnswers: <SurveyItemAnswer>[EXAMPLE_SURVEY_ITEM_ANSWER_TEST_1, EXAMPLE_SURVEY_ITEM_ANSWER_TEST_2]);

SurveyItem exampleSurveyItem2 = SurveyItem(
    name: 'test name 2',
    question: 'test question 2',
    possibleAnswers: <SurveyItemAnswer>[EXAMPLE_SURVEY_ITEM_ANSWER_TEST_1, EXAMPLE_SURVEY_ITEM_ANSWER_TEST_2]);

SurveyItem exampleSurveyItem3 = SurveyItem(
    name: 'test name 3',
    question: 'test question 3',
    possibleAnswers: <SurveyItemAnswer>[EXAMPLE_SURVEY_ITEM_ANSWER_TEST_1, EXAMPLE_SURVEY_ITEM_ANSWER_TEST_2]);

void main() {
  test('serialize and deserialize survey item', () {
    final SurveyItem surveyItem = exampleSurveyItem1;

    final String encodedAnswer = jsonEncode(surveyItem.toMap());

    final SurveyItem decodedAnswer = SurveyItem.fromMap(jsonDecode(encodedAnswer) as Map<String, dynamic>);

    expect(decodedAnswer, surveyItem);
  });

  test('does not allow duplicate item answer names', () {
    expect(
        () => SurveyItem(
            name: 'test name',
            question: 'test question',
            possibleAnswers: <SurveyItemAnswer>[EXAMPLE_SURVEY_ITEM_ANSWER_TEST_1, EXAMPLE_SURVEY_ITEM_ANSWER_TEST_1]),
        throwsA(const TypeMatcher<ItemAnswerNamesNotUniqueException>()));
  });
}
