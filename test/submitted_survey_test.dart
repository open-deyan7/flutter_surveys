import 'dart:convert';

import 'package:flutter_surveys/model/submitted_survey.dart';
import 'package:flutter_surveys/model/survey_item.dart';
import 'package:flutter_surveys/model/survey_item_answer.dart';
import 'package:flutter_test/flutter_test.dart';

import 'survey_test.dart';

SubmittedSurvey exampleSubmittedSurvey = SubmittedSurvey(
    submissionTime: DateTime(2022, 01, 12, 10, 12, 22, 321),
    survey: exampleSurvey,
    answers: <String, String>{
      exampleSurvey.items[0].name: exampleSurvey.items[0].possibleAnswers[0].name,
      exampleSurvey.items[1].name: exampleSurvey.items[1].possibleAnswers[1].name,
      exampleSurvey.items[2].name: exampleSurvey.items[2].possibleAnswers[0].name
    });

void main() {
  test('serialize and deserialize submitted survey', () {
    final SubmittedSurvey submittedSurvey = exampleSubmittedSurvey;

    final String encodedSubmittedSurvey = jsonEncode(submittedSurvey.toMap());

    final SubmittedSurvey decodedSubmittedSurvey =
        SubmittedSurvey.fromMap(jsonDecode(encodedSubmittedSurvey) as Map<String, dynamic>);

    expect(decodedSubmittedSurvey, submittedSurvey);
  });

  test('returns correct item to answer mapping when calling mappedAnswers ', () {
    final SubmittedSurvey submittedSurvey = exampleSubmittedSurvey;

    // has an answer for every item
    expect(submittedSurvey.mappedAnswers.length, exampleSubmittedSurvey.survey.items.length);

    for (final SurveyItem surveyItem in submittedSurvey.survey.items) {
      final SurveyItemAnswer? answer = submittedSurvey.mappedAnswers[surveyItem];
      expect(answer, isNotNull);
      expect(answer!.name, exampleSubmittedSurvey.answers[surveyItem.name]);
    }
  });

  test('only allows submitted surveys which have an answer for each item', () {
    expect(
        () => SubmittedSurvey(
                submissionTime: DateTime(2022, 01, 12, 10, 12, 22, 321),
                survey: exampleSurvey,
                answers: <String, String>{
                  exampleSurvey.items[0].name: exampleSurvey.items[0].possibleAnswers[0].name,
                  exampleSurvey.items[2].name: exampleSurvey.items[2].possibleAnswers[0].name
                }),
        throwsA(const TypeMatcher<NotAllItemsAnsweredException>()));
  });
}
