import 'dart:convert';

import 'package:flutter_surveys/model/survey.dart';
import 'package:flutter_surveys/model/survey_item.dart';
import 'package:flutter_test/flutter_test.dart';

import 'survey_item_test.dart';

Survey exampleSurvey = Survey(
    name: 'test name',
    explanation: 'test explanation',
    items: <SurveyItem>[exampleSurveyItem1, exampleSurveyItem2, exampleSurveyItem3],
    allowSkip: true);

void main() {
  test('serialize and deserialize survey', () {
    final Survey survey = exampleSurvey;

    final String encodedSurvey = jsonEncode(survey.toMap());

    final Survey decodedSurvey = Survey.fromMap(jsonDecode(encodedSurvey) as Map<String, dynamic>);

    expect(decodedSurvey, survey);
  });

  test('does not allow duplicate item names', () {
    expect(
        () => Survey(
            name: 'test name',
            explanation: 'test explanation',
            items: <SurveyItem>[exampleSurveyItem1, exampleSurveyItem2, exampleSurveyItem1],
            allowSkip: false),
        throwsA(const TypeMatcher<ItemNamesNotUniqueException>()));
  });

  test('default value of allowSkip if omitted from json is false', () {
    final Survey survey = exampleSurvey;

    final Map<String, dynamic> surveyMap = survey.toMap();
    surveyMap.remove(Survey.PROPERTY_ALLOW_SKIP);
    final String encodedSurvey = jsonEncode(surveyMap);

    final Survey decodedSurvey = Survey.fromMap(jsonDecode(encodedSurvey) as Map<String, dynamic>);

    expect(exampleSurvey.allowSkip, true);
    expect(decodedSurvey.allowSkip, false);
  });
}
