## 0.0.2

- add a "start now" call to action on survey initialization page, which will go to the first unanswered item or the
  summary page (in case everything is already answered)
- selected answers are not in a slightly different color (buttonTheme.primaryVariant as opposed to butonTheme.primary)
  and have a checkmark icon
- added option to surveys to be skipped by the user
- added method `fillOutSurvey` to the package, which opens an overlay and returns a `SurveyResult`, which contains the
  potentially answered survey + details on user action (cancel / skip)
- Implemented summary widget with edit functionality

## 0.0.1

- define surveys in code or json
- store and load from json
- widgets for performing a survey and collecting the result
