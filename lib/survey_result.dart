import 'package:flutter_surveys/model/submitted_survey.dart';

class SurveyResult {
  SurveyResult({this.submittedSurvey, required this.userActionDetails});

  final SubmittedSurvey? submittedSurvey;

  final UserAction userActionDetails;
}

enum UserAction { canceled_survey, selected_not_now, survey_submitted }
