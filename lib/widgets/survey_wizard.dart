import 'package:flutter/material.dart';
import 'package:flutter_surveys/flutter_surveys.dart';
import 'package:flutter_surveys/model/submitted_survey.dart';
import 'package:flutter_surveys/model/survey.dart';
import 'package:flutter_surveys/model/survey_item.dart';
import 'package:flutter_surveys/model/survey_item_answer.dart';
import 'package:flutter_surveys/widgets/survey_introduction.dart';
import 'package:flutter_surveys/widgets/survey_item_widget.dart';
import 'package:flutter_surveys/widgets/survey_summary.dart';

class SurveyWizard extends StatefulWidget {
  const SurveyWizard({Key? key, required this.survey}) : super(key: key);

  final Survey survey;

  @override
  State<SurveyWizard> createState() => _SurveyWizardState();
}

class _SurveyWizardState extends State<SurveyWizard> with TickerProviderStateMixin {
  Map<String, String> _answers = <String, String>{};

  final PageController _pageController = PageController(viewportFraction: 1);
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _answers = <String, String>{};
    _tabController = TabController(length: widget.survey.items.length + 2, vsync: this);
  }

  @override
  void dispose() {
    _pageController.dispose();
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: TabPageSelector(
            controller: _tabController,
          ),
        ),
        Expanded(
          child: PageView(
              controller: _pageController,
              onPageChanged: (int newPage) => _tabController.index = newPage,
              children: <Widget>[
                SurveyIntroduction(
                  survey: widget.survey,
                  onStartNowTap: _navigateToNextImportantPage,
                  onNotNowTapped: () {
                    Navigator.pop(context, SurveyResult(userActionDetails: UserAction.selected_not_now));
                  },
                ),
                ..._surveyItemWidgets(),
                SurveySummary(
                    onEditItemPressed: (SurveyItem item) => _navigateToItem(item),
                    answers: Map<SurveyItem, SurveyItemAnswer?>.fromIterable(widget.survey.items,
                        value: (dynamic item) => _currentAnswerForItem(item as SurveyItem)),
                    onSavePressed: _answersComplete() ? () => Navigator.pop(context, _buildSurveyResult()) : null)
              ]
                  .map((Widget pageViewChild) => SingleChildScrollView(
                        child: pageViewChild,
                      ))
                  .toList()),
        ),
      ],
    );
  }

  bool _answersComplete() {
    return _firstItemWithoutAnswer() == null;
  }

  SurveyResult _buildSurveyResult() => SurveyResult(
      userActionDetails: UserAction.survey_submitted,
      submittedSurvey: SubmittedSurvey(submissionTime: DateTime.now(), survey: widget.survey, answers: _answers));

  List<SurveyItemWidget> _surveyItemWidgets() {
    return widget.survey.items
        .map((SurveyItem item) => SurveyItemWidget(
            item: item,
            currentAnswer: _currentAnswerForItem(item),
            onAnswerChosen: (SurveyItemAnswer answer) {
              setState(() {
                _answers[item.name] = answer.name;
              });

              _navigateToNextImportantPage();
            }))
        .toList();
  }

  SurveyItemAnswer? _currentAnswerForItem(SurveyItem item) {
    return item.possibleAnswers
        .cast<SurveyItemAnswer?>()
        .firstWhere((SurveyItemAnswer? answer) => _answers[item.name] == answer?.name, orElse: () => null);
  }

  void _navigateToNextImportantPage() {
    final SurveyItem? firstItemWithoutAnswer = _firstItemWithoutAnswer();
    if (firstItemWithoutAnswer != null) {
      _navigateToItem(firstItemWithoutAnswer);
    } else {
      final int indexOfSummaryPage = widget.survey.items.length + 1;
      _animateToPage(indexOfSummaryPage);
    }
  }

  void _navigateToItem(SurveyItem firstItemWithoutAnswer) {
    final int indexOfNextUnansweredPage = widget.survey.items.indexOf(firstItemWithoutAnswer) + 1;
    _animateToPage(indexOfNextUnansweredPage);
  }

  Future<void> _animateToPage(int page) {
    return _pageController.animateToPage(page, duration: const Duration(milliseconds: 200), curve: Curves.easeInOut);
  }

  /// Returns null when all answers are given, otherwise the first in survey.items without answer in _answers
  SurveyItem? _firstItemWithoutAnswer() {
    try {
      return widget.survey.items.firstWhere((SurveyItem item) => !_answers.containsKey(item.name));
    } on StateError catch (_) {
      return null;
    }
  }
}
