import 'package:flutter/material.dart';
import 'package:flutter_surveys/model/survey_item.dart';
import 'package:flutter_surveys/model/survey_item_answer.dart';

import 'survey_button.dart';

class SurveyItemWidget extends StatelessWidget {
  const SurveyItemWidget({Key? key, required this.item, required this.onAnswerChosen, required this.currentAnswer})
      : super(key: key);

  final SurveyItem item;

  final SurveyItemAnswer? currentAnswer;

  final ValueChanged<SurveyItemAnswer> onAnswerChosen;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          item.question,
          style: Theme.of(context).textTheme.headline5,
        ),
        _buildAnswersForItem(item)
      ],
    );
  }

  Widget _buildAnswersForItem(SurveyItem item) {
    return Column(
        children: item.possibleAnswers.map((SurveyItemAnswer answer) {
      return SurveyButton(
          onButtonPressed: () {
            onAnswerChosen(answer);
          },
          highlight: currentAnswer == answer, // highlight if this is the currently active answer
          label: answer.label);
    }).toList());
  }
}
