import 'package:flutter/widgets.dart' as w;
import 'package:flutter_surveys/flutter_surveys.dart';
import 'package:flutter_surveys/model/survey.dart';
import 'package:flutter_surveys/survey_result.dart';
import 'package:flutter_surveys/widgets/overlay.dart';
import 'package:flutter_surveys/widgets/survey_wizard.dart';

class SurveyOverlay extends Overlay<SurveyResult> {
  SurveyOverlay({required this.survey})
      : super(
            onCloseButtonTap: (w.BuildContext context) {
              w.Navigator.pop(context, SurveyResult(userActionDetails: UserAction.canceled_survey));
            },
            child: SurveyWizard(survey: survey));

  final Survey survey;
}
