import 'package:flutter/material.dart';

class SurveyButton extends StatelessWidget {
  const SurveyButton({Key? key, this.onButtonPressed, required this.label, this.highlight = false}) : super(key: key);

  final VoidCallback? onButtonPressed;
  final String label;

  /// highlights the button (for instance for chosen answers)
  final bool highlight;

  @override
  Widget build(BuildContext context) {
    final ButtonStyle? buttonStyle = _determineButtonStyle(context);

    return Container(
      height: 70,
      width: double.infinity,
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
      child: highlight
          ? ElevatedButton.icon(
              onPressed: onButtonPressed, label: Text(label), style: buttonStyle, icon: const Icon(Icons.check_box))
          : ElevatedButton(
              onPressed: onButtonPressed,
              child: Text(label),
              style: buttonStyle,
            ),
    );
  }

  ButtonStyle? _determineButtonStyle(BuildContext context) {
    final ColorScheme? buttonColorTheme = Theme.of(context).buttonTheme.colorScheme;
    if (buttonColorTheme != null) {
      if (highlight) {
        final Color buttonColor = buttonColorTheme.primary;
        return ElevatedButton.styleFrom(primary: buttonColor);
      } else {
        final Color buttonColor = buttonColorTheme.primary;

        return ElevatedButton.styleFrom(primary: buttonColor);
      }
    }
    return null;
  }
}
