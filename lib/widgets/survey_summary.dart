import 'package:flutter/material.dart';
import 'package:flutter_surveys/model/survey_item.dart';
import 'package:flutter_surveys/model/survey_item_answer.dart';
import 'package:flutter_surveys/widgets/survey_button.dart';

class SurveySummary extends StatelessWidget {
  const SurveySummary({Key? key, required this.onSavePressed, required this.answers, required this.onEditItemPressed})
      : super(key: key);

  final VoidCallback? onSavePressed;

  final ValueChanged<SurveyItem> onEditItemPressed;

  final Map<SurveyItem, SurveyItemAnswer?> answers;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Zusammenfassung',
                textAlign: TextAlign.left,
                style: Theme.of(context).textTheme.headline4,
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: SurveyButton(
            onButtonPressed: onSavePressed,
            label: 'Speichern',
          ),
        ),
        ...answers
            .map((SurveyItem key, SurveyItemAnswer? value) => MapEntry<SurveyItem, Widget>(
                key,
                Card(
                  child: ListTile(
                    title: Text(key.question),
                    subtitle: Text(value?.label ?? '---'),
                    trailing: IconButton(
                      icon: const Icon(Icons.edit),
                      onPressed: () => onEditItemPressed(key),
                    ),
                  ),
                )))
            .values
            .toList(),
        const Padding(padding: EdgeInsets.only(bottom: 50))
      ],
    );
  }
}
