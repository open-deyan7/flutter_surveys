import 'package:flutter/material.dart';
import 'package:flutter_surveys/model/survey.dart';
import 'package:flutter_surveys/widgets/survey_button.dart';

class SurveyIntroduction extends StatelessWidget {
  const SurveyIntroduction({Key? key, required this.survey, required this.onStartNowTap, required this.onNotNowTapped})
      : super(key: key);

  final Survey survey;

  final VoidCallback onStartNowTap;

  final VoidCallback onNotNowTapped;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: Text(
            'Fragebogen',
            style: Theme.of(context).textTheme.headline2,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 40.0, left: 30, right: 30),
          child: Text(
            survey.explanation,
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: SurveyButton(
            onButtonPressed: onStartNowTap,
            label: 'Jetzt beginnen',
            highlight: true,
          ),
        ),
        if (survey.allowSkip)
          Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: SurveyButton(onButtonPressed: onNotNowTapped, label: 'Jetzt nicht'),
          ),
      ],
    );
  }
//
//
// List<Widget> _buildSummaryItems() {
//   return survey.items.map((SurveyItem item) {
//     final SurveyItemAnswer answer =
//     item.possibleAnswers.firstWhere((SurveyItemAnswer answerName) => answerName.name == _answers[item.name]);
//     return Padding(
//       padding: const EdgeInsets.symmetric(vertical: 4.0),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.end,
//         children: <Widget>[
//           Text('${item.question}: ${answer.label}'),
//           IconButton(
//               onPressed: () => setState(() {
//                 _answers.remove(item.name);
//               }),
//               icon: const Icon(Icons.edit))
//         ],
//       ),
//     );
//   }).toList();
// }
}
