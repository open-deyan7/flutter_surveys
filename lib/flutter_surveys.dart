library flutter_surveys;

import 'package:flutter/material.dart';
import 'package:flutter_surveys/survey_result.dart';

import 'model/survey.dart';
import 'widgets/survey_overlay.dart';

export 'package:flutter_surveys/survey_result.dart';

Future<SurveyResult> fillOutSurvey(BuildContext context, Survey survey, {bool allowSkip = false}) async {
  final SurveyResult? result = await Navigator.of(context).push(SurveyOverlay(survey: survey));
  if (result != null) {
    return result;
  } else {
    return SurveyResult(userActionDetails: UserAction.canceled_survey);
  }
}
