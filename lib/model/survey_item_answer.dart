import 'package:flutter/foundation.dart';

class SurveyItemAnswer {
  const SurveyItemAnswer({required this.name, required this.label, this.additionalData = const <String, dynamic>{}});

  SurveyItemAnswer.fromMap(Map<String, dynamic> map)
      : this(
            name: map[PROPERTY_NAME] as String,
            label: map[PROPERTY_LABEL] as String,
            additionalData: map[PROPERTY_ADDITIONAL_DATA] as Map<String, dynamic>);

  /// must be unique within the SurveyItem it belongs to
  final String name;

  final String label;

  final Map<String, dynamic> additionalData;

  Map<String, dynamic> toMap() {
    return <String, dynamic>{PROPERTY_NAME: name, PROPERTY_LABEL: label, PROPERTY_ADDITIONAL_DATA: additionalData};
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SurveyItemAnswer &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          label == other.label &&
          mapEquals<String, dynamic>(additionalData, other.additionalData);

  @override
  int get hashCode => name.hashCode ^ label.hashCode ^ additionalData.hashCode;

  @override
  String toString() {
    return 'SurveyItemAnswer{name: $name, label: $label, additionalData: $additionalData}';
  }

  static const String PROPERTY_NAME = 'name';
  static const String PROPERTY_LABEL = 'label';
  static const String PROPERTY_ADDITIONAL_DATA = 'additional_data';
}
