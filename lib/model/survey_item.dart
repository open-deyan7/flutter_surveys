import 'package:flutter/foundation.dart';
import 'package:flutter_surveys/model/survey_item_answer.dart';

class SurveyItem {
  /// Throws ItemAnswerNamesNotUniqueException if the possible answers contain duplicate names
  SurveyItem({required this.name, required this.question, required List<SurveyItemAnswer> possibleAnswers})
      : possibleAnswers = _checkAnswerNamesUnique(possibleAnswers);

  SurveyItem.fromMap(Map<String, dynamic> map)
      : this(
            name: map[PROPERTY_NAME] as String,
            question: map[PROPERTY_QUESTION] as String,
            possibleAnswers: (map[PROPERTY_POSSIBLE_ANSWERS] as List<dynamic>).map<SurveyItemAnswer>((dynamic e) {
              final Map<String, dynamic> answerMap = e as Map<String, dynamic>;
              return SurveyItemAnswer.fromMap(answerMap);
            }).toList());

  static List<SurveyItemAnswer> _checkAnswerNamesUnique(List<SurveyItemAnswer> answers) {
    if (Set<String>.from(answers.map((SurveyItemAnswer e) => e.name).toList()).length != answers.length) {
      throw ItemAnswerNamesNotUniqueException();
    }
    return answers;
  }

  /// must be unique within the Survey it belongs to
  final String name;

  final String question;

  final List<SurveyItemAnswer> possibleAnswers;

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      PROPERTY_NAME: name,
      PROPERTY_QUESTION: question,
      PROPERTY_POSSIBLE_ANSWERS: possibleAnswers.map((SurveyItemAnswer e) => e.toMap()).toList()
    };
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SurveyItem &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          question == other.question &&
          listEquals(possibleAnswers, other.possibleAnswers);

  @override
  String toString() {
    return 'SurveyItem{name: $name, question: $question, possibleAnswers: $possibleAnswers}';
  }

  @override
  int get hashCode => name.hashCode ^ question.hashCode ^ possibleAnswers.hashCode;

  static const String PROPERTY_NAME = 'name';
  static const String PROPERTY_QUESTION = 'question';
  static const String PROPERTY_POSSIBLE_ANSWERS = 'possible_answers';
}

class ItemAnswerNamesNotUniqueException implements Exception {}
