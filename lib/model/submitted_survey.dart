import 'package:flutter/foundation.dart';
import 'package:flutter_surveys/model/survey.dart';
import 'package:flutter_surveys/model/survey_item.dart';
import 'package:flutter_surveys/model/survey_item_answer.dart';

class SubmittedSurvey {
  /// Throws NotAllItemsAnsweredException if the answers map does not have an answer for each item in the survey
  SubmittedSurvey({required this.submissionTime, required this.survey, required Map<String, String> answers})
      : answers = _validateAllItemsAnswered(answers, survey);

  SubmittedSurvey.fromMap(Map<String, dynamic> map)
      : this(
            submissionTime: DateTime.parse(map[PROPERTY_SUBMISSION_TIME] as String),
            survey: Survey.fromMap(map[PROPERTY_SURVEY] as Map<String, dynamic>),
            answers: (map[PROPERTY_ANSWERS] as Map<String, dynamic>)
                .map((String key, dynamic value) => MapEntry<String, String>(key, value as String)));

  final DateTime submissionTime;

  final Survey survey;

  /// maps SurveyItem names to the name of the SurveyItemAnswer that was chosen
  final Map<String, String> answers;

  /// maps the survey questions to the chosen answer, as object
  Map<SurveyItem, SurveyItemAnswer> get mappedAnswers {
    return _mapNamesToItemAndAnswers(answers, survey);
  }

  static Map<String, String> _validateAllItemsAnswered(Map<String, String> answers, Survey survey) {
    final Map<SurveyItem, SurveyItemAnswer> mapNamesToItemAndAnswers = _mapNamesToItemAndAnswers(answers, survey);
    if (mapNamesToItemAndAnswers.length != survey.items.length) {
      throw NotAllItemsAnsweredException();
    }
    return answers;
  }

  static Map<SurveyItem, SurveyItemAnswer> _mapNamesToItemAndAnswers(Map<String, String> answers, Survey survey) {
    return answers.map((String key, String value) {
      final SurveyItem surveyItem = survey.items.firstWhere((SurveyItem element) => element.name == key);
      final SurveyItemAnswer surveyItemAnswer =
          surveyItem.possibleAnswers.firstWhere((SurveyItemAnswer element) => element.name == value);
      return MapEntry<SurveyItem, SurveyItemAnswer>(surveyItem, surveyItemAnswer);
    });
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SubmittedSurvey &&
          runtimeType == other.runtimeType &&
          submissionTime == other.submissionTime &&
          survey == other.survey &&
          mapEquals(answers, other.answers);

  @override
  int get hashCode => submissionTime.hashCode ^ survey.hashCode ^ answers.hashCode;

  @override
  String toString() {
    return 'SubmittedSurvey{submissionTime: $submissionTime, survey: $survey, answers: $answers}';
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      PROPERTY_SUBMISSION_TIME: submissionTime.toIso8601String(),
      PROPERTY_SURVEY: survey.toMap(),
      PROPERTY_ANSWERS: answers
    };
  }

  static const String PROPERTY_SUBMISSION_TIME = 'submission_time';
  static const String PROPERTY_SURVEY = 'survey';
  static const String PROPERTY_ANSWERS = 'answers';
}

class NotAllItemsAnsweredException implements Exception {}
