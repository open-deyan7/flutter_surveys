import 'package:flutter/foundation.dart';
import 'package:flutter_surveys/model/survey_item.dart';

class Survey {
  Survey({required this.name, required this.explanation, required List<SurveyItem> items, required this.allowSkip})
      : items = _checkItemNamesUnique(items);

  Survey.fromMap(Map<String, dynamic> map)
      : this(
          name: map[PROPERTY_NAME] as String,
          explanation: map[PROPERTY_EXPLANATION] as String,
          items: (map[PROPERTY_ITEMS] as List<dynamic>).map<SurveyItem>((dynamic e) {
            final Map<String, dynamic> itemMap = e as Map<String, dynamic>;
            return SurveyItem.fromMap(itemMap);
          }).toList(),
          allowSkip: map.containsKey(PROPERTY_ALLOW_SKIP) && map[PROPERTY_ALLOW_SKIP] as bool,
        );

  static List<SurveyItem> _checkItemNamesUnique(List<SurveyItem> items) {
    if (Set<String>.from(items.map((SurveyItem e) => e.name).toList()).length != items.length) {
      throw ItemNamesNotUniqueException();
    }
    return items;
  }

  final String name;

  final String explanation;

  final List<SurveyItem> items;

  final bool allowSkip;

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      PROPERTY_NAME: name,
      PROPERTY_EXPLANATION: explanation,
      PROPERTY_ITEMS: items.map((SurveyItem e) => e.toMap()).toList(),
      PROPERTY_ALLOW_SKIP: allowSkip
    };
  }

  @override
  String toString() {
    return 'Survey{name: $name, explanation: $explanation, items: $items, allowSkip: $allowSkip}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Survey &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          explanation == other.explanation &&
          listEquals(items, other.items) &&
          allowSkip == other.allowSkip;

  @override
  int get hashCode => name.hashCode ^ explanation.hashCode ^ items.hashCode ^ allowSkip.hashCode;

  static const String PROPERTY_NAME = 'name';
  static const String PROPERTY_EXPLANATION = 'explanation';
  static const String PROPERTY_ITEMS = 'items';
  static const String PROPERTY_ALLOW_SKIP = 'allow_skip';
}

class ItemNamesNotUniqueException implements Exception {}
