A flutter package to define, store and perform surveys

## Features

- define surveys in code or json
- store and load from json
- widgets for performing a survey and collecting the result

## Getting started

### Model

A survey consists of the following objects

#### Survey Item

A question in the survey. The name of the item must be unique within the survey it belongs to

#### Survey Item Answer

An answer to a question in a survey. The name of the answer must be unique within the survey item it belongs to

Answers can contain an additional map of `additionalData`, for instance for scoring surveys.

#### Survey

A collection of survey items.

Also contains name and explanation of the survey.

#### SubmittedSurvey

A survey that has been answered and submitted by the user.

Contains a copy of the survey, so it is easy to store and be sure about the survey version that was answered

## Usage

You can define the survey in code, or as a json file.

Example for definition in code:

```dart

String healthSurveyIntroduction = '''
This survey assesses your general health 

Please tell us how you felt during the last 24 hours.''';

Survey survey = Survey(
  name: 'general_health',
  explanation: healthSurveyIntroduction,
  items: allItems,
);

final healthItem1 = SurveyItem(name: 'health_item_active', question: 'active', possibleAnswers: surveyAnswers);
final healthItem2 = SurveyItem(name: 'health_item_tired', question: 'tired', possibleAnswers: surveyAnswers);
final healthItem2 = SurveyItem(name: 'health_item_in_pain', question: 'in pain', possibleAnswers: surveyAnswers);


final List<SurveyItem> allItems = [healthItem1, healthItem2, healthItem3];


const SurveyItemAnswer healthAnswer1 = SurveyItemAnswer(name: '1', label: 'Not at all');
const SurveyItemAnswer healthAnswer2 = SurveyItemAnswer(name: '2', label: 'A little bit');
const SurveyItemAnswer healthAnswer3 = SurveyItemAnswer(name: '3', label: 'Somewhat');
const SurveyItemAnswer healthAnswer4 = SurveyItemAnswer(name: '4', label: 'Strongly');
const SurveyItemAnswer healthAnswer5 = SurveyItemAnswer(name: '5', label: 'Very Strongly');

const List<SurveyItemAnswer> surveyAnswers = <SurveyItemAnswer>[
  healthAnswer1,
  healthAnswer2,
  healthAnswer3,
  healthAnswer4,
  healthAnswer5
];
```

The survey can then be displayed with a method like this this:

```dart

Future<SurveyResult> fillOutSurvey(BuildContext context, Survey survey) async {
  return await fillOutSurvey(context, survey);
}
```

`SurveyResult` contains the result of the survey in submittedSurvey variable, or `null` if it was not filled out. Also
it provides details on the action the users take (skip if allowed, cancel)

